package dwi.bagas.appx06

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var fragProdi : FragmentProdi
    lateinit var fragMhs : FragmentMahasiswa
    lateinit var fragPembagian : FragmentPembagian
    lateinit var fragKali : FragmentPerkalian
    lateinit var ft : FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragProdi = FragmentProdi()
        fragMhs = FragmentMahasiswa()
        fragPembagian = FragmentPembagian()
        fragKali = FragmentPerkalian()
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when(p0.itemId){
            R.id.itemProdi ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragProdi).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemMhs ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragMhs).commit()
                frameLayout.setBackgroundColor(Color.argb(245,225,255,255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemPembagian ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragPembagian).commit()
                frameLayout.setBackgroundColor(Color.argb(255,245,245,215))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemKali ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragKali).commit()
                frameLayout.setBackgroundColor(Color.argb(255,245,245,215))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemAbout -> frameLayout.visibility = View.GONE
        }
        return true
    }
}

